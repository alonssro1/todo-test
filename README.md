# Todo Test

This project is built using React and Vite, targeting Node.js version 20. It's designed to offer a fast development environment and an optimized build setup.

## Prerequisites

Before you begin, ensure you have Node.js version 20 or later installed on your system. You can check your Node version by running `node -v` in your terminal.

## Installation

To set up the project locally, follow these steps:

1. Clone the repository to your local machine:
  ```console
  git clone <repository-url>
  ```

2. Navigate into the project directory:
  ```console
  cd todo-test
  ```

3. Install the dependencies:
```console
  npm install
```

## Running the Project
After installing the project dependencies, you can run the project in development mode:
```console
  npm run build
```
Vite will generate the production build in the dist folder. You can serve the production build by running:

```console
  npm run preview
```

This command serves the production build on a local static server. Check the terminal for the URL to access the application.


## Editing the Project

After installing the project dependencies, you can run the project in development mode:

```console
npm run dev
````

This command starts the Vite development server. Open http://localhost:3000 to view your project in the browser. The page will automatically reload if you make edits.

All source files are inside `/src` folder


Contributing
Contributions are welcome! Please read the contributing guidelines before submitting pull requests to the project.

License