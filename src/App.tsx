import React, { useEffect, useState } from "react";
import { Filter, ToDo } from "./types";
import { ToDoList } from "./components";

const App: React.FC = () => {
  const [todos, setTodos] = useState<ToDo[]>([]);
  const [newTodo, setNewTodo] = useState<string>('');
  const [filter, setFilter] = useState<Filter>('all');

  // Load todos from localStorage on initial render
  useEffect(() => {
    const savedTodos = localStorage.getItem('todos');
    if (savedTodos) {
      setTodos(JSON.parse(savedTodos));
    }
  }, []);

  // Save todos to localStorage whenever todos change
  useEffect(() => {
    localStorage.setItem('todos', JSON.stringify(todos));
  }, [todos]);

  // Add new todo to the list
  const addTodo = () => {
    if (newTodo.trim() !== '') {
      setTodos([...todos, { text: newTodo, completed: false }]);
      setNewTodo('');
    }
  };

  // Filter todos based on the selected filter
  const getFilteredTodos = (): ToDo[] => {
    switch (filter) {
      case 'active':
        return todos.filter(todo => !todo.completed);
      case 'completed':
        return todos.filter(todo => todo.completed);
      case 'all':
      default:
        return todos;
    }
  };

  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-gray-100">
      <div className="App bg-white p-4 rounded-lg shadow-md w-full max-w-md">
        <h1 className="text-3xl font-bold mb-4 text-center">To-Do List</h1>
        <div className="flex mb-4">
          <input 
            type="text" 
            value={newTodo} 
            onChange={(e) => setNewTodo(e.target.value)} 
            placeholder="Add a new task"
            className="p-2 border border-gray-300 rounded-l w-full focus:outline-none focus:ring focus:border-blue-500"
          />
          <button 
            onClick={addTodo}
            className="p-2 bg-blue-500 text-white rounded-r hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-500"
          >
            Add
          </button>
        </div>
        <div className="flex justify-center mb-4">
          <button 
            onClick={() => setFilter('all')}
            className={`px-4 py-2 rounded-l ${filter === 'all' ? 'bg-blue-500 text-white' : 'bg-gray-200 text-gray-800'} hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-500`}
          >
            All
          </button>
          <button 
            onClick={() => setFilter('active')}
            className={`px-4 py-2 ${filter === 'active' ? 'bg-blue-500 text-white' : 'bg-gray-200 text-gray-800'} hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-500`}
          >
            Active
          </button>
          <button 
            onClick={() => setFilter('completed')}
            className={`px-4 py-2 rounded-r ${filter === 'completed' ? 'bg-blue-500 text-white' : 'bg-gray-200 text-gray-800'} hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-500`}
          >
            Completed
          </button>
        </div>
        <ToDoList todos={getFilteredTodos()} setTodos={setTodos} />
      </div>
    </div>
  );
}

export default App;
