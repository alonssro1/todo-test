import React from "react";
import { ToDoListProps } from "../types";
import { ToDoItem } from "./TodoItem";

export const ToDoList: React.FC<ToDoListProps> = ({ todos, setTodos }) => {
  const toggleComplete = (index: number) => {
    const newTodos = [...todos];
    newTodos[index].completed = !newTodos[index].completed;
    setTodos(newTodos);
  };

  return (
    <ul className="divide-y divide-gray-300">
      {todos.map((todo, index) => (
        <ToDoItem 
          key={index} 
          todo={todo} 
          toggleComplete={() => toggleComplete(index)} 
        />
      ))}
    </ul>
  );
};
