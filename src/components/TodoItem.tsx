import React from 'react';
import { ToDoItemProps } from '../types';

export const ToDoItem: React.FC<ToDoItemProps> = ({ todo, toggleComplete }) => {
  return (
    <li className="flex items-center justify-between border-b border-gray-300 py-2">
      <span
        className={`flex-grow ml-2 ${todo.completed ? 'line-through text-gray-500' : 'text-gray-800'}`}
      >
        {todo.text}
      </span>
      <button
        onClick={toggleComplete}
        className={`px-2 py-1 rounded ${todo.completed ? 'bg-yellow-300 text-gray-800' : 'bg-green-500 text-white'}`}
      >
        {todo.completed ? 'Undo' : 'Complete'}
      </button>
    </li>
  );
};
