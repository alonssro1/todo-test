export interface ToDo {
  text: string;
  completed: boolean;
}
export interface ToDoListProps {
  todos: ToDo[];
  setTodos: React.Dispatch<React.SetStateAction<ToDo[]>>;
}
export interface ToDoItemProps {
  todo: ToDo;
  toggleComplete: () => void;
}

export type Filter = 'all' | 'active' | 'completed';
